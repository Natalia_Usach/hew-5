fetch('https://jsonplaceholder.typicode.com/users')
  .then((response) => response.json())
  .then((json) => getUsers(json));

const usersList = document.getElementById('list');
const spinner = document.getElementById('spinner');
let newUserName;

function showSpinner() {
    spinner.className = 'show';
    setTimeout(() => {
      spinner.className = spinner.className.replace('show', '');
    }, 2000);
  }

function getUsers(data) {
    showSpinner();
    let userName, userId;
    for(let i = 0; i < data.length; i++) {
            userId = data[i].id;
            userName = data[i].username;
        usersList.innerHTML += `<div class='container'><span>${userId}</span>
        <li>${userName}</li>
        <button class='delete-btn'>Delete</button></div>`;
    }

const listItems = document.querySelectorAll('li');

for(let item of listItems) {
    item.addEventListener('click', function(event) {
        const newId = item.previousSibling.textContent;
        document.body.innerHTML = `<textarea>${event.target.textContent}</textarea>
        <button class='submit-edit-btn'>Submit edit</button>`;
        document.querySelector('.submit-edit-btn').addEventListener('click', function() {
            newUserName = document.querySelector('textarea').value;
            fetch(`https://jsonplaceholder.typicode.com/users/${newId}`, {
                method: 'PUT',
                body: JSON.stringify({
                id: newId,
                name: 'Leanne Graham',
                username: newUserName,
                email: 'Sincere@april.biz',
                address: {
                    street: 'Kulas Light',
                    suite: 'Apt. 556',
                    city: 'Gwenborough',
                    zipcode: '92998-3874',
                    geo: {
                    lat: '-37.3159',
                    lng: '81.1496'
                    }
                },
                phone: '1-770-736-8031 x56442',
                website: 'hildegard.org',
                company: {
                    name: 'Romaguera-Crona',
                    catchPhrase: 'Multi-layered client-server neural-net',
                    bs: 'harness real-time e-markets'
                }
                }),
                headers: {
                'Content-type': 'application/json; charset=UTF-8'
                }
          })
            .then((response) => response.json())
            .then((json) => console.log(json))
            .then(window.location.reload());

        });
    });
}

const delButtons = document.querySelectorAll('.delete-btn');
for (let el of delButtons) {

    el.addEventListener('click', function(e) {
        let numId = e.target.parentNode.firstChild.textContent;
        fetch(`https://jsonplaceholder.typicode.com/users/${numId}`, {
            method: 'DELETE'
        });
        window.location.reload();
    });
}
}